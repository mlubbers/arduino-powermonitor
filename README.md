# Arduino Power Monitor

## Tested hardware

LOLIN Wemos D1 Mini (esp8266) with the adafruit INA260 current sensing breakout board and the OLED 0.66 shield.

## Set up

- Make sure that the folder of the repo is called `powermon` (i.e. run `git clone https://gitlab.science.ru.nl/mlubbers/arduino-powermonitor.git powermon`).
- Install the INA260 library from the library manager.
- Install the ESP async TCP library from here: https://github.com/me-no-dev/ESPAsyncTCP
- Install the ESP async Web Server library from here: https://github.com/me-no-dev/ESPAsyncWebServer
- Install the OLED 0.66 shield library from here: https://github.com/mcauser/Adafruit_SSD1306/tree/esp8266-64x48.
- Copy `wificonfig.h.def` to `wificonfig.h`.
- Open `powermon.ino`.
- Fill in the WiFi details in `wificonfig.h`.
- Compile and run.

## Usage

The hostname and IP address of the device are printed on the OLED screen.

### With an mDNS resolver (linux, mac, windows)

Navigate to the hostname of the device to view the current graph.

### Without an mDNS resolver (android)

Navigate to the IP address of the device to view the current graph.
