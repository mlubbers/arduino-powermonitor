#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include "wificonfig.h"

/*
 * ESP8266WiFi.h: No such file or directory
 */
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "website.h"
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

//#include <Adafruit_INA260.h>
//Adafruit_INA260 ina260 = Adafruit_INA260();

#include <Wire.h>
#include <INA226_WE.h>
#define I2C_ADDRESS 0x40

INA226_WE INA = INA226_WE(I2C_ADDRESS);

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define OLED_RESET 0
Adafruit_SSD1306 display(OLED_RESET);

long lastreading = 0;
long tbr = 1000;

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo*)arg;
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
    tbr = atof((char *)data)*1000;
    Serial.printf("new measuring interval: %ld", tbr);
  }
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
    switch (type) {
      case WS_EVT_CONNECT:
        Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
        break;
      case WS_EVT_DISCONNECT:
        Serial.printf("WebSocket client #%u disconnected\n", client->id());
        break;
      case WS_EVT_DATA:
        handleWebSocketMessage(arg, data, len);
        break;
      case WS_EVT_PONG:
      case WS_EVT_ERROR:
        Serial.printf("Websocket client #%u error\n", client->id());
        break;
  }
  (void)server;
}

void initWebSocket() {
  ws.onEvent(onEvent);
  server.addHandler(&ws);
}

String processor(const String& var){
  if (var.equals("DEFAULT_TBR"))
    return String(tbr/1000.0);
  return var;
}

void setup(){
  Serial.begin(115200);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

  display.clearDisplay();
  display.display();

  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.setTextSize(1);

//  if (!ina260.begin()) {
//    Serial.println("Couldn't find INA260 chip");
//    display.println("Couldn't find INA260 chip");
//    display.display();
//    while (1);
//  }
    Wire.begin();
    INA.init();

//  Serial.println("INA260 detected");
  Serial.println("INA226 detected");
  Serial.println("Connecting to WiFi");

  WiFi.mode(WIFI_STA);
  WiFi.setSleepMode(WIFI_LIGHT_SLEEP);

  int idx = -1;
  do {
      display.clearDisplay();
      display.setCursor(0, 0);
      display.display();
      Serial.println("Scanning...");
      display.println("Scanning..");
      int n = WiFi.scanNetworks();
      Serial.printf("Found %d networks\n", n);
      display.printf("Found %d\n", n);
      display.display();
      if (n == 0) {
        Serial.println("No networks found");
        delay(1000);
        continue;
      }
      for (unsigned j = 0; j<sizeof(wifi_networks)/sizeof(struct wifi_info); j++) {
        for (int i = 0; i < n; i++) {
          if (strcmp(WiFi.SSID(i).c_str(), wifi_networks[j].ssid) == 0) {
            Serial.printf("Try\n%s\n", WiFi.SSID(i).c_str());
            display.printf("Try\n%.10s\n", WiFi.SSID(i).c_str());
            display.display();
            idx = j;
          }
        }
      }
    } while (idx == -1);
  WiFi.scanDelete();
  WiFi.begin(wifi_networks[idx].ssid, wifi_networks[idx].password);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    display.print(".");
    display.display();
    delay(1000);
  }
  Serial.println();

  display.clearDisplay();
  display.setCursor(0, 0);
  display.printf("%.10s\n", wifi_networks[idx].ssid);

  Serial.println(WiFi.localIP());
  display.printf("%d.%d\n.%d.%d\n", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
  display.display();

  if (!MDNS.begin(hostname)) {
    Serial.println("Error setting up mDNS responder!");
    display.println("mDNS error!");
    display.display();
    while (1);
  }
  Serial.printf("addr: %s.local\n", hostname);
  display.printf("addr:\n%s\n.local\n", hostname);
  display.display();

  initWebSocket();

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  server.begin();
}

void loop() {
  MDNS.update();
  ws.cleanupClients();

  long t = millis();
  if(lastreading + tbr < t) {
//    float current = ina260.readCurrent();
    float current = INA.getCurrent_mA();
    ws.printfAll("[%f, %f]", t/1000.0, current);
    lastreading += tbr;
  }
  delay(75);
}
