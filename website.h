const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>Power monitor</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://code.highcharts.com/highcharts.src.js"></script>
</head>
<body>
  <label for="freq">Measuring interval (s)</label>
  <input id="freq" type="number" value="%DEFAULT_TBR%" min="0.1" step="0.1" max="60.0" onchange="websocket.send(document.getElementById('freq').value)" />
  <div id="container" style="width:100%; height:400px;"></div>

  <script>
    let chart;
    var gateway = `ws://${window.location.hostname}/ws`;
    let websocket;
    function initWebSocket() {
      websocket = new WebSocket(gateway);
      websocket.onmessage = onMessage;
      websocket.onclose = onClose;
      websocket.onopen = onOpen;
      websocket.onerror = onError;
    }
    function onOpen(event) {
      console.log('Connection opened');
      console.log(event);
    }
    function onClose(event) {
      console.log('Connection closed');
      console.log(event);
      setTimeout(initWebSocket, 500);
    }
    function onError(event) {
      console.log('Connection Errored');
      console.log(event);
      websocket.close();
      setTimeout(initWebSocket, 500);
    }
    function onMessage(event) {
      const series = chart.series[0],
        shift = series.data.length > 100; // shift if the series is longer than 20
      chart.series[0].addPoint(JSON.parse(event.data), true, shift);
    }
    document.addEventListener('DOMContentLoaded', function () {
        chart = Highcharts.chart('container', {
            chart: {
                type: 'line',
                animation: false
            },
            title: {
                text: 'Power consumption'
            },
            xAxis: {
                title: {
                  text: 'time since boot in ms'
                }
            },
            yAxis: {
                title: {
                    text: 'mA'
                }
            },
            series: [{
                name: 'Current',
                data: []
            }]
        });
      console.log('Trying to open a WebSocket connection...');
      initWebSocket();
    });
    </script>
</body>
</html>
)rawliteral";
